import java.util.ArrayList;
import java.util.Scanner;

public class Main
{
    private static Scanner sc = new Scanner(System.in);
    private static GroceryList groceryList = new GroceryList();

    public static void main(String[] args)
    {
        boolean quit = false;
        int choice = 0;
        printInstructions();
        while(quit == false)
        {
            System.out.println("Enter your choice: ");
            choice = sc.nextInt();
            sc.nextLine();

            switch(choice){
                case 0:
                    printInstructions();
                    break;
                case 1:
                    groceryList.printGroceryList();
                    break;
                case 2:
                    addItem();
                    break;
                case 3:
                    modifyItem();
                    break;
                case 4:
                    removeItem();
                    break;
                case 5:
                    searchItem();
                    break;
                case 6:
                    quit = true;
                    break;
            }
        }
    }
    public static void printInstructions()
    {
        System.out.println("\nPress");
        System.out.println("\t 0 - To print choice options");
        System.out.println("\t 1 - To print grocery list");
        System.out.println("\t 2 - To add item to grocery list");
        System.out.println("\t 3 - To modify the list");
        System.out.println("\t 4 - To remove an item");
        System.out.println("\t 5 - Search for an item");
        System.out.println("\t 6 - To quit the application");
    }

    public static void addItem()
    {
        System.out.println("Please enter the grocery item");
        groceryList.addGroceryItem(sc.nextLine().toLowerCase());
    }

    public static void modifyItem()
    {
        System.out.println("Enter the item name: ");
        String oldItem = sc.nextLine();
        System.out.println("Enter replacement item: ");
        String newItem = sc.nextLine();
        groceryList.modifyGroceryItem(oldItem, newItem);
    }

    public static void removeItem()
    {
        System.out.println("Enter the item name: ");
        String itemToRemove = sc.nextLine();
        groceryList.removeGroceryItem(itemToRemove);
    }

    public static void searchItem()
    {
        System.out.println("Please enter item to search for");
        String searchItem = sc.nextLine();
        if(groceryList.inList(searchItem.toLowerCase()))
        {
            System.out.println("Found " + searchItem);
        }
        else
        {
            System.out.println(searchItem + " not found");
        }
    }

    public static void processArrayList()
    {
        ArrayList<String> newArray = new ArrayList<>();
        newArray.addAll(groceryList.getGroceryList());

        ArrayList<String> nextArray = new ArrayList<>(groceryList.getGroceryList());

        String[] myArray = new String[groceryList.getGroceryList().size()];
        myArray = groceryList.getGroceryList().toArray(myArray);

    }
}
